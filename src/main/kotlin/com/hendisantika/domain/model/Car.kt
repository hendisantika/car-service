package com.hendisantika.domain.model

import java.io.Serializable

/**
 * Created by IntelliJ IDEA.
 * Project : car-service
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/23/22
 * Time: 06:57
 * To change this template use File | Settings | File Templates.
 */
data class Car(
    val id: Long,
    val name: String,
    val model: String,
    val year: Long,
    val isEligible: Boolean? = null
) : Serializable {

    fun isEligibleToUber(): Car {
        return if (year >= 2008) {
            this.copy(isEligible = true)
        } else {
            this.copy(isEligible = false)
        }
    }
}
