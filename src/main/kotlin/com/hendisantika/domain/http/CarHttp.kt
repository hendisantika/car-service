package com.hendisantika.domain.http

/**
 * Created by IntelliJ IDEA.
 * Project : car-service
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/23/22
 * Time: 06:56
 * To change this template use File | Settings | File Templates.
 */
data class CarHttp(
    val make: String,
    val model: String,
    val year: Long
)