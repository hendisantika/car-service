package com.hendisantika.core.converter

import com.hendisantika.domain.http.CarHttp
import com.hendisantika.domain.model.Car

/**
 * Created by IntelliJ IDEA.
 * Project : car-service
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/23/22
 * Time: 07:01
 * To change this template use File | Settings | File Templates.
 */
object CarHttpToModelConverter {
    fun toModel(carsHttp: List<CarHttp>) =
        carsHttp.map { carHttp ->
            Car(
                id = Long.MAX_VALUE,
                name = carHttp.model,
                model = carHttp.make,
                year = carHttp.year
            ).isEligibleToUber()
        }
}