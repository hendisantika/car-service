package com.hendisantika.core.service

import com.hendisantika.adapter.http.CarHttpService
import com.hendisantika.core.converter.CarHttpToModelConverter
import com.hendisantika.domain.model.Car
import com.hendisantika.domain.port.CarRepository
import com.hendisantika.domain.port.CarService
import kotlinx.coroutines.coroutineScope
import org.springframework.cache.annotation.CacheEvict
import org.springframework.cache.annotation.Cacheable
import org.springframework.stereotype.Service
import retrofit2.awaitResponse

/**
 * Created by IntelliJ IDEA.
 * Project : car-service
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/23/22
 * Time: 07:02
 * To change this template use File | Settings | File Templates.
 */
@Service
internal class CarService(
    private val carRepository: CarRepository,
    private val carHttpService: CarHttpService
) : CarService {
    @Cacheable(cacheNames = ["Cars"], key = "#root.method.name")
    override fun list(model: String?) =
        model?.let {
            carRepository.listByModel(it)
        } ?: carRepository.listAll()

    @CacheEvict(cacheNames = ["Cars"], allEntries = true)
    override fun save(car: Car) =
        car.isEligibleToUber().let { carRepository.save(it) }

    @CacheEvict(cacheNames = ["Cars"], allEntries = true)
    override fun update(car: Car, id: Long) = carRepository.update(car, id)

    override fun findById(id: Long): Car {
        return carRepository.findById(id) ?: throw RuntimeException()
    }

    override suspend fun listByNinjaAPI(model: String): List<Car>? = coroutineScope {
        carHttpService
            .getByModel(model)
            .awaitResponse()
            .body()
            ?.let { carHttp -> CarHttpToModelConverter.toModel(carHttp) }
    }
}