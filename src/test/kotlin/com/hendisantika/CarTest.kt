package com.hendisantika

import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe

/**
 * Created by IntelliJ IDEA.
 * Project : car-service
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/25/22
 * Time: 18:11
 * To change this template use File | Settings | File Templates.
 */
class CarTest : FunSpec(
    {
        val car = CarFixture.getCar()

        test("should return isEligible when year is greater than 2008") {
            val actual = car.isEligibleToUber()

            actual.isEligible shouldBe true
        }
        test("should return isNotEligible when year is less than 2008") {
            val actual = car.copy(year = 2007).isEligibleToUber()

            actual.isEligible shouldBe false
        }
    }
)