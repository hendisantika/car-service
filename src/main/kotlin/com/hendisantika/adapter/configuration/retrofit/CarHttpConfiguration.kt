package com.hendisantika.adapter.configuration.retrofit

import com.hendisantika.adapter.configuration.circuitbreaker.CircuitBreakerConfiguration
import com.hendisantika.adapter.http.CarHttpService
import io.github.resilience4j.retrofit.CircuitBreakerCallAdapter
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by IntelliJ IDEA.
 * Project : car-service
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/23/22
 * Time: 07:46
 * To change this template use File | Settings | File Templates.
 */
@Configuration
class CarHttpConfiguration(
    @Value("\${api-ninja.key}") private val key: String,
    private val circuitBreakerConfiguration: CircuitBreakerConfiguration
) {
    private companion object {
        const val BASE_URL = "https://api.api-ninjas.com/v1/"
    }

    private fun buildClient() = OkHttpClient.Builder().apply {
        addInterceptor(
            Interceptor interceptor@{ chain ->
                val builder = chain.request().newBuilder().apply {
                    header("X-Api-Key", key)
                }.build()
                return@interceptor chain.proceed(builder)
            }
        )
    }.build()

    private fun buildRetrofit() = Retrofit.Builder()
        .addCallAdapterFactory(CircuitBreakerCallAdapter.of(circuitBreakerConfiguration.getCircuitBreaker()))
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .client(buildClient())
        .build()


    @Bean
    fun carHttpService(): CarHttpService = buildRetrofit().create(CarHttpService::class.java)
}