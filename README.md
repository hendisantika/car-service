# Car Service Kotlin

### Things todo list

1. Clone this repository: `git clone https://gitlab.com/hendisantika/car-service.git`
2. Navigate to the folder: `cd car-service`
3. Run the application: `mvn clean spring-boot:run`
4. Open your terminal

Add New Cars

```shell
curl --location --request POST 'localhost:8080/cars' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "Lancer",
    "model": "sedan",
    "year": "2021",
    "isEligible": true
}'
```

Get All Cars

```shell
curl --location --request GET 'localhost:8080/cars'
```

Update Car

```shell
curl --location --request PUT 'localhost:8080/cars/5' \
--header 'Content-Type: application/json' \
--data-raw '{
        "name": "L300",
        "model": "Pickup",
        "year": 2010,
        "isEligible": true
    }'
```

