package com.hendisantika.adapter.bd

import com.hendisantika.domain.model.Car
import org.springframework.jdbc.core.RowMapper
import java.sql.ResultSet

/**
 * Created by IntelliJ IDEA.
 * Project : car-service
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/23/22
 * Time: 07:08
 * To change this template use File | Settings | File Templates.
 */
class CarMapper : RowMapper<Car> {

    override fun mapRow(rs: ResultSet, rowNum: Int) = Car(
        id = requireNotNull(rs.getLong("id")),
        name = requireNotNull(rs.getString("name")),
        model = requireNotNull(rs.getString("model")),
        year = requireNotNull(rs.getLong("year_car")),
        isEligible = rs.getBoolean("is_eligible")
    )
}