package com.hendisantika.adapter.bd

import com.hendisantika.domain.model.Car
import com.hendisantika.domain.port.CarRepository
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Repository

/**
 * Created by IntelliJ IDEA.
 * Project : car-service
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/23/22
 * Time: 07:09
 * To change this template use File | Settings | File Templates.
 */
@Repository
class CarRepository(
    private val jdbcTemplate: JdbcTemplate
) : CarRepository {
    override fun listAll(): List<Car> = jdbcTemplate.query("SELECT * FROM car", CarMapper())

    override fun listByModel(model: String): List<Car> =
        jdbcTemplate.query("SELECT * FROM car WHERE model = ?", CarMapper(), model)

    override fun save(car: Car) = jdbcTemplate.update(
        "INSERT INTO car(name, model, year_car, is_eligible) VALUES(?,?,?,?)",
        car.name,
        car.model,
        car.year,
        car.isEligible
    )

    override fun update(car: Car, id: Long) = jdbcTemplate.update(
        "UPDATE car SET name = ?, model = ?, year_car = ? WHERE id = ?",
        car.name,
        car.model,
        car.year,
        id
    )

    override fun findById(id: Long): Car? =
        jdbcTemplate.queryForObject("SELECT * FROM car WHERE id = ?", CarMapper(), id)
}