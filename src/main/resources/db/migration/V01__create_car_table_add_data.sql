CREATE TABLE if NOT EXISTS car
(
    id
    bigint
    NOT
    NULL
    auto_increment,
    NAME
    VARCHAR
(
    50
) NOT NULL,
    model VARCHAR
(
    50
) NOT NULL,
    PRIMARY KEY
(
    id
)
    );

INSERT INTO car
VALUES (1, 'Golf', 'VW');