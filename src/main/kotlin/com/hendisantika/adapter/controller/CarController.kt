package com.hendisantika.adapter.controller

import com.hendisantika.domain.model.Car
import com.hendisantika.domain.port.CarService
import kotlinx.coroutines.runBlocking
import org.springframework.web.bind.annotation.*

/**
 * Created by IntelliJ IDEA.
 * Project : car-service
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/25/22
 * Time: 06:29
 * To change this template use File | Settings | File Templates.
 */

@RestController
@RequestMapping("/cars")
class CarController(private val carService: CarService) {
    @GetMapping
    fun list(@RequestParam(required = false) model: String?) = carService.list(model)

    @PostMapping
    fun save(@RequestBody car: Car) = carService.save(car)

    @PutMapping("/{id}")
    fun update(@RequestBody car: Car, @PathVariable id: Long) = carService.update(car, id)

    @GetMapping("/ninja-api")
    fun listByNinjaAPI(@RequestParam model: String) = runBlocking {
        carService.listByNinjaAPI(model)
    }
}