package com.hendisantika.adapter.configuration.circuitbreaker

import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig
import io.github.resilience4j.circuitbreaker.CircuitBreakerRegistry
import org.springframework.context.annotation.Configuration
import java.time.Duration

/**
 * Created by IntelliJ IDEA.
 * Project : car-service
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/23/22
 * Time: 07:44
 * To change this template use File | Settings | File Templates.
 */
@Configuration
class CircuitBreakerConfiguration {

    fun getConfiguration() =
        CircuitBreakerConfig
            .custom()
            .slidingWindowType(CircuitBreakerConfig.SlidingWindowType.COUNT_BASED)
            .slidingWindowSize(10)
            .slowCallRateThreshold(70.0f)
            .slowCallDurationThreshold(Duration.ofSeconds(2))
            .waitDurationInOpenState(Duration.ofSeconds(5000))
            .permittedNumberOfCallsInHalfOpenState(10)
            .writableStackTraceEnabled(false)
            .build()

    fun getCircuitBreaker() =
        CircuitBreakerRegistry.of(getConfiguration())
            .circuitBreaker("CIRCUIT-BREAKER")
}