package com.hendisantika

import com.hendisantika.domain.model.Car

/**
 * Created by IntelliJ IDEA.
 * Project : car-service
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/25/22
 * Time: 18:10
 * To change this template use File | Settings | File Templates.
 */
object CarFixture {
    fun getCar() = Car(1, "Gol", "VW", year = 2015)
}