package com.hendisantika.domain.port

import com.hendisantika.domain.model.Car

/**
 * Created by IntelliJ IDEA.
 * Project : car-service
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/23/22
 * Time: 07:00
 * To change this template use File | Settings | File Templates.
 */
interface CarService {
    fun list(model: String?): List<Car>
    fun save(car: Car): Int
    fun update(car: Car, id: Long): Int
    fun findById(id: Long): Car
    suspend fun listByNinjaAPI(model: String): List<Car>?
}