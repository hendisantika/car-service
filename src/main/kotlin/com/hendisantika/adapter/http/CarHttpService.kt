package com.hendisantika.adapter.http

import com.hendisantika.domain.http.CarHttp
import org.springframework.stereotype.Service
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by IntelliJ IDEA.
 * Project : car-service
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/23/22
 * Time: 07:06
 * To change this template use File | Settings | File Templates.
 */
@Service
interface CarHttpService {

    @GET("cars")
    suspend fun getByModel(@Query("model") model: String): Call<List<CarHttp>>
}